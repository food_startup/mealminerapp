import { take, put, call } from 'redux-saga/effects'
import Types from '../Actions/Types'
import Actions from '../Actions/Creators'

// a helper for simulating work
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

export default (api) => {

  // attempts to login
   function * attemptLogin (username, password) {
    // simulate work
    console.log('hallo')
    yield put(Actions.loginFailure(false))
    const response = yield call(api.login, username,password)
    console.log(response)

    if (response.status !== 200) {
      // dispatch failure
      yield put(Actions.loginFailure(true))
    } else {
      // dispatch successful logins
      token = response.data.token
      yield put(Actions.loginSuccess(username, token))
      yield put(Actions.loginFailure(false))

      //THIS ABSOLUTELY SHOULDNT BE HERE
      yield put (Actions.fetchMeals())
    }
  }

  // a daemonized version which waits for LOGIN_ATTEMPT signals
  function * watchLoginAttempt () {
    // daemonize
    while (true) {
      // wait for LOGIN_ATTEMPT actions to arrive
      const { username, password } = yield take(Types.LOGIN_ATTEMPT)
      // call attemptLogin to perform the actual work
      yield call(attemptLogin, username, password)
    }
  }

  function * attemptRegister (email, password) {
   // simulate work
   console.log('hallowwwww')

   yield put(Actions.registerFailure(false))
   const response = yield call(api.register, email,password)
   console.log(response)

   if (response.status !== 201) {
     // dispatch failure
     yield put(Actions.registerFailure(true))

   } else {
     //now log in the user
     yield put (Actions.attemptLogin(email, password))
   }
 }


  function * watchRegisterAttempt () {
    // daemonize
    while (true) {
      // wait for LOGIN_ATTEMPT actions to arrive
      const { email, password } = yield take(Types.REGISTER_ATTEMPT)
      // call attemptLogin to perform the actual work
      yield call(attemptRegister, email, password)
    }
  }

  return {
    attemptLogin,
    watchLoginAttempt,
    attemptRegister,
    watchRegisterAttempt,
  }
}
