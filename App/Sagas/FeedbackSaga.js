import { take, put, call } from 'redux-saga/effects'
import Types from '../Actions/Types'
import Actions from '../Actions/Creators'

import Metrics from '../Themes/Metrics'

import ImageResizer from 'react-native-image-resizer';


// a helper for simulating work
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

function downSize(imgURI) {

}

export default (api) => {



  function * submitFeedback (feedback, token) {
    //yield put(Actions.patchCurrentMeal(patch))
    //const response = yield call(api.searchDish, query, token)
    //console.log(response)

    const response = yield call(api.submitFeedback, feedback, token)
    //console.log(response)

  }


  function * submitFeedbackWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { feedback, token } = yield take(Types.SUBMIT_FEEDBACK)
      // call attemptLogin to perform the actual work
      yield call(submitFeedback, feedback, token)
    }
  }

  return {
    submitFeedback,
    submitFeedbackWatcher
  }
}
