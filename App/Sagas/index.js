import { fork } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import { watchStartup } from './StartupSaga'
import loginSaga from './LoginSaga'
import mealSaga from './MealSaga'
import dishSaga from './DishSaga'
import feedbackSaga from './FeedbackSaga'
import getCityWeather from './GetCityWeatherSaga'
import DebugSettings from '../Config/DebugSettings'
import MEALMINERAPI from '../Services/MealoMiAPI'


// Create our API at this level and feed it into
// the sagas that are expected to make API calls
// so there's only 1 copy app-wide!
// const api = API.create()

if (__DEV__) {
  var baseURL = 'http://mealminer-dev.eu-central-1.elasticbeanstalk.com/api'
}
else {
  var baseURL = 'http://mealminer-prod.eu-central-1.elasticbeanstalk.com/api'
}


const api = DebugSettings.useFixtures ? FixtureAPI : API.create()


const MealoMiAPI = MEALMINERAPI.create(baseURL)

// start the daemons
export default function * root () {
  yield fork(watchStartup)
  yield fork(loginSaga(MealoMiAPI).watchLoginAttempt)
  yield fork(loginSaga(MealoMiAPI).watchRegisterAttempt)
  yield fork(mealSaga(MealoMiAPI).newMealWatcher)
  yield fork(mealSaga(MealoMiAPI).fetchMealsWatcher)
  yield fork(mealSaga(MealoMiAPI).fetchMealWatcher)
  yield fork(mealSaga(MealoMiAPI).patchMealWatcher)
  yield fork(mealSaga(MealoMiAPI).deleteMealWatcher)
  yield fork(dishSaga(MealoMiAPI).searchDishWatcher)
  yield fork(feedbackSaga(MealoMiAPI).submitFeedbackWatcher)



  yield fork(getCityWeather(api).watcher)
}
