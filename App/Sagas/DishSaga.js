import { take, put, call } from 'redux-saga/effects'
import Types from '../Actions/Types'
import Actions from '../Actions/Creators'

import Metrics from '../Themes/Metrics'

import ImageResizer from 'react-native-image-resizer';


// a helper for simulating work
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

function downSize(imgURI) {

}

export default (api) => {



  function * searchDish (query, token) {
    //yield put(Actions.patchCurrentMeal(patch))
    const response = yield call(api.searchDish, query, token)
    console.log(response)

    if (response.status === 200) {

      yield put(Actions.receiveSearchResults(response.data))
    }
  }


  function * searchDishWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { query, token } = yield take(Types.SEARCH_DISH)
      // call attemptLogin to perform the actual work
      yield call(searchDish, query, token)
    }
  }

  return {
    searchDish,
    searchDishWatcher
  }
}
