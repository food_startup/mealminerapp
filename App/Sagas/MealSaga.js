import { take, put, call } from 'redux-saga/effects'
import Types from '../Actions/Types'
import Actions from '../Actions/Creators'

import Metrics from '../Themes/Metrics'


// a helper for simulating work
const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

function downSize(imgURI) {

}

export default (api) => {

  //creates a new meal
  function * newMeal (photoURI, token) {

    var photo = {
      uri: photoURI,
      type: 'image/jpeg',
      name: 'photo.jpg',
    };
    var body = new FormData();
    body.append('image', photo);
    const response = yield call(api.newMeal, body, token)
    //console.log('request', body)
    //console.log(response)
    if (response.status === 201) {
      console.log(response.data.name)
      yield put(Actions.fetchMeal(response.data.id))

    }
  }

  // a daemonized version which waits for LOGIN_ATTEMPT signals
  function * newMealWatcher () {
    // daemonize
    while (true) {
      // wait for LOGIN_ATTEMPT actions to arrive
      const { photo, token } = yield take(Types.TOOK_PHOTO)
      // call attemptLogin to perform the actual work
      yield call(newMeal, photo, token)
    }
  }



  //loads list of meals
  function * fetchMeals (token) {
    const response = yield call(api.fetchMeals, token)
    console.log(response)

    if (response.status === 200) {
      yield put(Actions.mealsReceived(response.data))
    }
  }


  function * fetchMealsWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { token } = yield take(Types.FETCH_MEALS)
      // call attemptLogin to perform the actual work
      yield call(fetchMeals, token)
    }
  }

  //loads a single meal
  function * fetchMeal (id, token) {
    console.log('now Im fetching')
    const response = yield call(api.fetchMeal, id, token)
    console.log(response)

    if (response.status === 200) {
      yield put(Actions.setCurrentMeal(response.data))
    }
  }


  function * fetchMealWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { id, token } = yield take(Types.FETCH_MEAL)
      // call attemptLogin to perform the actual work
      yield call(fetchMeal, id, token)
    }
  }


  //TODO shoud be called patchLinkedDish
  function * patchMeal (mealID, patch, token) {
    console.log('attempting to patch')
    yield put(Actions.patchCurrentMeal(patch))
    console.log('patch vorher', patch)
    patch = {linked_dish: patch.linked_dish.url}
    console.log('patch nachher', patch)
    const response = yield call(api.patchMeal, mealID, patch, token)
    console.log(response)
    if (response.status === 200) {
      //fuck it. we just do optimistic loading for the sake of perceived snappieness.
      //yield put(Actions.patchCurrentMeal(patch))
    }
  }


  function * patchMealWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { mealID, patch, token } = yield take(Types.PATCH_MEAL)
      // call attemptLogin to perform the actual work
      yield call(patchMeal, mealID, patch, token)
    }
  }

  function * deleteMeal (mealID, token) {
    console.log('attempting to delete')
    //yield put(Actions.patchCurrentMeal(patch))
    const response = yield call(api.deleteMeal, mealID, token)
    yield call(fetchMeals, token)
    //console.log(response)
    //if (response.status === 200) {
      //fuck it. we just do optimistic loading for the sake of perceived snappieness.
      //yield put(Actions.patchCurrentMeal(patch))
    //}
  }


  function * deleteMealWatcher () {
    // daemonize
    while (true) {
      // wait for FETCH_MEALS actions to arrive
      const { mealID, token } = yield take(Types.DELETE_MEAL)
      // call attemptLogin to perform the actual work
      yield call(deleteMeal, mealID, token)
    }
  }

  return {
    newMeal,
    newMealWatcher,
    fetchMeals,
    fetchMealsWatcher,
    fetchMeal,
    fetchMealWatcher,
    patchMeal,
    patchMealWatcher,
    deleteMeal,
    deleteMealWatcher
  }
}
