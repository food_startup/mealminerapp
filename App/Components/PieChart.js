import React from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/PieChartStyle'

import {Colors, Fonts} from '../Themes'

export default class PieChart extends React.Component {

  // // Prop type warnings
  // static propTypes = {
  //   someProperty: React.PropTypes.object,
  //   someSetting: React.PropTypes.bool.isRequired
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    var perc = this.props.value
    var deg = 360*perc/100
    var base = this.props.size

    var inSideCol = 'white'
    var lineWidth = 30
    var offset = 180
    if(perc <33) {
      var col = '#C62828'
      var bgcol= '#E57373'
    }
    else if (perc < 66) {
      var col = '#F9A825'
      var bgcol= '#FFF176'
    }
    else {
      var col = '#2E7D32'
      var bgcol= '#81C784'
    }

    if(deg<90) {
      return(
        <View >
          <View style={{width:base, height:base, transform: [{rotate: offset + 'deg'}]}}>
            <View style={{backgroundColor:bgcol, width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base/2, borderTopRightRadius:base/2}} />
          </View>
          <View style={{transform: [{rotate: offset + deg + 'deg'}], width:base, height:base, position:'relative', top:-base}}>
            <View style={{backgroundColor:'transparent', width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:bgcol, width:base/2, height:base/2, borderTopRightRadius:base/2}} />
          </View>
          <View style={{justifyContent:'center', position:'absolute', top:lineWidth/2, left:lineWidth/2, borderRadius:(base-lineWidth)/2, width:base-lineWidth, height:base-lineWidth, backgroundColor:inSideCol}}>
            <Text style={{opacity:0.84, textAlign:'center', color: Colors.primaryText, fontSize: base/3,
            fontFamily: Fonts.style.display.fontFamily}}>{perc}</Text>
          </View>

        </View>
      )
    }
    else if (deg>90 && deg<180) {
      return (
        <View>
          <View style={{width:base, height:base, transform: [{rotate: offset + 'deg'}]}}>
            <View style={{backgroundColor:bgcol, width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base/2, borderTopRightRadius:base/2}} />
          </View>
          <View style={{transform: [{rotate: offset + (deg-90) + 'deg'}], width:base, height:base, position:'relative', top:-base}}>
            <View style={{backgroundColor:'transparent', width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base/2, borderTopRightRadius:base/2}} />
          </View>
          <View style={{justifyContent:'center', position:'absolute', top:lineWidth/2, left:lineWidth/2, borderRadius:(base-lineWidth)/2, width:base-lineWidth, height:base-lineWidth, backgroundColor:inSideCol}}>
            <Text style={{opacity:0.84, textAlign:'center', color: Colors.primaryText, fontSize: base/3,
            fontFamily: Fonts.style.display.fontFamily}}>{perc}</Text>
          </View>
        </View>
      )
    }
    else if (deg>=180 && deg<270) {
      return (
        <View>
          <View style={{width:base, height:base, transform: [{rotate: offset + 'deg'}]}}>
            <View style={{backgroundColor:bgcol, width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base, borderTopRightRadius:base/2, borderBottomRightRadius:base/2}} />
          </View>
          <View style={{transform: [{rotate: offset+ (deg-90) + 'deg'}], width:base, height:base, position:'relative', top:-base}}>
            <View style={{backgroundColor:'transparent', width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base/2, borderTopRightRadius:base/2}} />
          </View>
          <View style={{justifyContent:'center', position:'absolute', top:lineWidth/2, left:lineWidth/2, borderRadius:(base-lineWidth)/2, width:base-lineWidth, height:base-lineWidth, backgroundColor:inSideCol}}>
            <Text style={{opacity:0.84, textAlign:'center', color: Colors.primaryText, fontSize: base/3,
            fontFamily: Fonts.style.display.fontFamily}}>{perc}</Text>
          </View>

        </View>
      )
    }
    else if (deg>=270 && deg<=360) {
      return (
        <View>
          <View style={{width:base, height:base, transform: [{rotate: offset + 'deg'}]}}>
            <View style={{backgroundColor:bgcol, width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base, borderTopRightRadius:base/2, borderBottomRightRadius:base/2}} />
          </View>
          <View style={{transform: [{rotate: offset + (deg+180) + 'deg'}], width:base, height:base, position:'relative', top:-base}}>
            <View style={{backgroundColor:'transparent', width:base, height:base, borderRadius:base/2}} />
            <View style={{position:'relative', top:-base, left:base/2, backgroundColor:col, width:base/2, height:base, borderTopRightRadius:base/2, borderBottomRightRadius:base/2}} />
          </View>
          <View style={{justifyContent:'center', position:'absolute', top:lineWidth/2, left:lineWidth/2, borderRadius:(base-lineWidth)/2, width:base-lineWidth, height:base-lineWidth, backgroundColor:inSideCol}}>
            <Text style={{opacity:0.84, textAlign:'center', color: Colors.primaryText, fontSize: base/3,
            fontFamily: Fonts.style.display.fontFamily}}>{perc}</Text>
          </View>
        </View>
      )
    }

  }
}
