
//refer to https://material.google.com/what-is-material/elevation-shadows.html#elevation-shadows-elevation-android-
const elevations = {
  appBar: 4,
  bottomNavBar: 8,
  FAB: 5,
  Card:2,
  Dialogue:24,
}

export default elevations
