const colors = {

  //Material Design Colors
  //darkPrimary: '#388E3C',
  darkPrimary: '#689F38',
  defaultPrimary: '#8BC34A',
  //defaultPrimary: '#4CAF50',
  //lightPrimary: '#C8E6C9',
  textPrimary: '#FFFFFF',
  //accent: '#FFC107',
  accent: '#FFC107',
  primaryText: '#212121',
  secondaryText: '#727272',
  divider: '#B6B6B6',

  lightCardBackground: '#E8F5E9', //Green 50

  //Ignite Default Colors
  background: 'white',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536'
}

export default colors
