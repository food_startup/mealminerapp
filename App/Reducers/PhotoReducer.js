import Types from '../Actions/Types'
import Immutable from 'seamless-immutable'
import { createReducer } from 'reduxsauce'

export const INITIAL_STATE = Immutable({
  photo: 'hallo'
})

// incoming photo
const handlePhoto = (state, action) =>
  state.merge({photo: action.photo})

// map our types to our handlers
const ACTION_HANDLERS = {
  [Types.TOOK_PHOTO]: handlePhoto,
}

export default createReducer(INITIAL_STATE, ACTION_HANDLERS)
