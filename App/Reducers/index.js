import { combineReducers } from 'redux'
import LoginReducer from './LoginReducer'
import WeatherReducer from './WeatherReducer'
import PhotoReducer from './PhotoReducer'
import MealsReducer from './MealsReducer'
import DishReducer from './DishReducer'

// glue all the reducers together into 1 root reducer
export default combineReducers({
  login: LoginReducer,
  weather: WeatherReducer,
  photo: PhotoReducer,
  meals: MealsReducer,
  dish: DishReducer,
})

// Put reducer keys that you do NOT want stored to persistence here
export const persistentStoreBlacklist = []
