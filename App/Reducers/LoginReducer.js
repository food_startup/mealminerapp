import Types from '../Actions/Types'
import Immutable from 'seamless-immutable'
import { createReducer } from 'reduxsauce'

export const INITIAL_STATE = Immutable({
  username: null,
  errorCode: null,
  attempting: false
})

// login attempts
const attempt = (state, action) =>
  state.merge({ attempting: true })

// successful logins
const success = (state, action) =>
  state.merge({ attempting: false, loginFailure: null, username: action.username, token: action.token })

// login failure
const failure = (state, action) =>
  state.merge({ attempting: false, loginFailure: action.fail })

const registerFailure = (state, action) =>
  state.merge({registerFailure: action.fail})

// logout
const logout = (state, action) =>
  state.merge({ username: null, token: null })

// map our types to our handlers
const ACTION_HANDLERS = {
  [Types.LOGIN_ATTEMPT]: attempt,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.REGISTER_FAILURE]: registerFailure,
}

export default createReducer(INITIAL_STATE, ACTION_HANDLERS)
