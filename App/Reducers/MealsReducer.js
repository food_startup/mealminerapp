import Types from '../Actions/Types'
import Immutable from 'seamless-immutable'
import { createReducer } from 'reduxsauce'

export const INITIAL_STATE = Immutable({
  log:[],
})

// incoming photo
const handleMealsReceived = (state, action) => ({...state, log: action.mealsArray})

const handleSetCurrentMeal = (state, action) => ({...state, current: action.meal})

const handlePatchCurrentMeal = (state,action) => ({...state, current: {...state.current, ...action.mealPatch}})





// map our types to our handlers
const ACTION_HANDLERS = {
  [Types.MEALS_RECEIVED]: handleMealsReceived,
  [Types.SET_CURRENT_MEAL]: handleSetCurrentMeal,
  [Types.PATCH_CURRENT_MEAL]: handlePatchCurrentMeal,
}

export default createReducer(INITIAL_STATE, ACTION_HANDLERS)
