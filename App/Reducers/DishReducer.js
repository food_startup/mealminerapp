import Types from '../Actions/Types'
import Immutable from 'seamless-immutable'
import { createReducer } from 'reduxsauce'

export const INITIAL_STATE = Immutable({

})

// incoming photo
const handleReceiveSearchResults = (state, action) =>
  state.merge({searchResults: action.results})





// map our types to our handlers
const ACTION_HANDLERS = {
  [Types.RECEIVE_SEARCH_RESULTS]: handleReceiveSearchResults,

}

export default createReducer(INITIAL_STATE, ACTION_HANDLERS)
