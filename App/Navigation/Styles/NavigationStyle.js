import {StyleSheet} from 'react-native'
import { Fonts, Metrics, Colors, Elevations } from '../../Themes/'

const NavigationStyle = StyleSheet.create({
  titleWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },

  navTitle: {
    color: Colors.snow,
    fontFamily: Fonts.style.title.fontFamily,
    fontSize: Fonts.style.title.fontSize,
    justifyContent: 'center',
    alignSelf: 'center',

  },
  searchInput: {
    color: Colors.snow,
    fontFamily: Fonts.style.title.fontFamily,
    fontSize: Fonts.style.title.fontSize,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  navSubtitle: {
    flex: 1,
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.base,
    alignSelf: 'center'
  },
  navButtonText: {
    color: Colors.snow,

    fontFamily: Fonts.bold,

  },
  navButtonLeftWrapper:{
    width:56,
    height:56,
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    padding:16,
  },
  navButtonLeft: {
    flex:1
  },
  navButtonRightWrapper:{

    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:16,
    paddingBottom:16,
    paddingRight:16,
    paddingLeft:8
  },
  navButtonRightAdditionalWrapper:{

    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:16,
    paddingBottom:16,
    paddingRight:8,
    paddingLeft:8

  },
  navButtonRight: {
    flex:1
  },
  navigationBar: {
    backgroundColor: Colors.defaultPrimary,
    height: Metrics.navBarHeight,
    elevation: Elevations.appBar
  }
})

export default NavigationStyle
