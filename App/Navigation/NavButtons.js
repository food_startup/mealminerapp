import React from 'react'
import { TouchableOpacity, Text, View} from 'react-native'
import styles from './Styles/NavigationStyle'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Colors, Metrics } from '../Themes'

// I18n
import I18n from '../I18n/I18n.js'

export default {

  backButton (onPressFunction) {
    return (
      <TouchableOpacity onPress={onPressFunction}>
        <View style={styles.navButtonLeftWrapper}>
        <Icon name='arrow-back'
          size={Metrics.icons.medium}
          color={Colors.snow}
          style={styles.navButtonLeft}
        />
        </View>
      </TouchableOpacity>
    )
  },

  hamburgerButton (onPressFunction) {
    return (
      <TouchableOpacity onPress={onPressFunction}>
      <View style={styles.navButtonLeftWrapper}>
        <Icon name='menu'
          size={Metrics.icons.medium}
          color={Colors.snow}
          style={styles.navButtonLeft}
        />
        </View>
      </TouchableOpacity>
    )
  },

  forgotPasswordButton (onPressFunction) {
    return (
      <TouchableOpacity onPress={onPressFunction}>
        <Text style={styles.navButtonText}>{I18n.t('forgotPassword')}</Text>
      </TouchableOpacity>
    )
  },


  searchButton (onPressFunction) {
    return (
      <TouchableOpacity onPress={onPressFunction}>
        <View style={styles.navButtonRightWrapper}>
          <Icon name='search'
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.navButtonRight}
          />
        </View>
      </TouchableOpacity>
    )
  },

  checkButton (onPressFunction) {
    return (
      <View style={{flexDirection:'row'}}>
      <TouchableOpacity onPress={onPressFunction.handleDelete}>
        <View style={styles.navButtonRightAdditionalWrapper}>
          <Icon name='delete'
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.navButtonRight}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={onPressFunction.handleDone}>
        <View style={styles.navButtonRightWrapper}>
          <Icon name='done'
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.navButtonRight}
          />
        </View>
      </TouchableOpacity>
      </View>
    )
  },

  profileButton (onPressFunction) {
    return (
      <TouchableOpacity onPress={onPressFunction}>
        <View style={styles.navButtonRightWrapper}>
          <Icon name='exit-to-app'
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.navButtonRight}
          />
        </View>
      </TouchableOpacity>
    )
  },

  flashButton (onPressFunction, state) {
    console.log('exec')
    switch (state) {
      case "AUTO":
        var icon='flash-auto'
        break;
      case "ON":
        var icon='flash-on'
        break;
      case "OFF":
        var icon='flash-off'
        break;
      default:
        var icon='flash-auto'
    }
    return (
      <TouchableOpacity onPress={onPressFunction}>
        <View style={styles.navButtonRightWrapper}>
          <Icon name={icon}
            size={Metrics.icons.medium}
            color={Colors.snow}
            style={styles.navButtonRight}
          />
        </View>
      </TouchableOpacity>
    )
  }

}
