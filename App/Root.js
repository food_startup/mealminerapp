import React from 'react'
import { View, Text, Navigator, StatusBar, TouchableNativeFeedback, BackAndroid } from 'react-native'
import {Router, Routes, NavigationBar} from './Navigation/'
import configureStore from './Store/Store'
import { Provider } from 'react-redux'
import Actions from './Actions/Creators'
import Drawer from 'react-native-drawer'
import DebugSettings from './Config/DebugSettings'
import './Config/PushConfig'

import {Metrics, Colors} from './Themes'
import LoginContainer from './Containers/LoginContainer'
import BottomBar from './Containers/BottomBar'

import codePush from "react-native-code-push";

import GoogleAnalytics from 'react-native-google-analytics-bridge';
GoogleAnalytics.setTrackerId('UA-80178982-1')

//Turn of Google Analytics in development mode
if(__DEV__) {
  GoogleAnalytics.setDryRun(true);
}


// Styles
import styles, {drawerStyles} from './Containers/Styles/RootStyle'

const store = configureStore()

export default class RNBase extends React.Component {

  componentWillMount () {
    const { dispatch } = store
    dispatch(Actions.startup())

    console.log('store', store.getState())

    BackAndroid.addEventListener('hardwareBackPress', function() {
         console.log('pressed back button')
    });


  }

  componentDidMount () {

    codePush.sync();

    GoogleAnalytics.trackScreenView('Home')
    GoogleAnalytics.trackEvent('testcategory', 'testaction');




    this.navigator.drawer = this.drawer
    this.nav = this.navigator
    //this is to pass the navigator prop to the bottom bar
    //really awful. but works ^^
    this.forceUpdate()


    //handle hardware backpress function
    handleBackPress = () => {
      if (this.navigator.getCurrentRoutes().length>1) {
        //transition back in route stack.
        this.navigator.pop()
        return true //i.e. don't exit the app
      }
      return false //i.e. exit the app
    }
    //bind handleBackPress to this so that we have navigator available as this.navigator.
    handleBackPress = handleBackPress.bind(this)
    //event listener
    BackAndroid.addEventListener('hardwareBackPress', handleBackPress);
    //Yeah. JavaScript sucks so many times, but this here is just frickin awesome!

  }

  renderDrawerContent () {
    return (
      <View style={{marginTop: 30, padding: 10}}>
        <Text style={{color: 'white'}}>
          Drawer Content Goes Here!
        </Text>
      </View>
    )
  }





  renderApp () {
    //console.disableYellowBox = !DebugSettings.yellowBox
    console.disableYellowBox = true
    return (
      <Provider store={store}>
        <View style={styles.applicationView}>
          <StatusBar
            barStyle='light-content'
          />




            <StatusBar
              backgroundColor={Colors.darkPrimary}
              barStyle="light-content"
            />


            <Navigator
              ref={(ref) => { this.navigator = ref }}
              initialRoute={Routes.LoginScreen}
              configureScene={Router.configureScene}
              renderScene={Router.renderScene}
              navigationBar={NavigationBar.render()}
              style={styles.container}
            />


          <BottomBar navigator={this.nav}/>
        </View>
      </Provider>
    )
  }

  render () {
    return this.renderApp()
  }
}
