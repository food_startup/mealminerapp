import Types from './Types'

//REMOVE
import Routes from '../Navigation/Routes'


const attemptLogin = (username, password) =>
  ({ type: Types.LOGIN_ATTEMPT, username, password })

const loginSuccess = (username, token) =>
  ({ type: Types.LOGIN_SUCCESS, username, token })

const loginFailure = (fail) =>
  ({ type: Types.LOGIN_FAILURE, fail })

const attemptRegister = (email, password) =>
  ({ type: Types.REGISTER_ATTEMPT, email, password })

const registerFailure = (fail) =>
  ({type: Types.REGISTER_FAILURE, fail})

const logout = () => ({ type: Types.LOGOUT })

const startup = () => ({ type: Types.STARTUP })

const requestTemperature = (city) => ({ type: Types.TEMPERATURE_REQUEST, city })
const receiveTemperature = (temperature) => ({ type: Types.TEMPERATURE_RECEIVE, temperature })
const receiveTemperatureFailure = () => ({ type: Types.TEMPERATURE_FAILURE })

const tookPhoto = (photo) => {
  return  {type: Types.TOOK_PHOTO, photo};}



const fetchMeals = () => {
  return {type: Types.FETCH_MEALS};
}
const fetchMeal = (id) => {
  return {type: Types.FETCH_MEAL, id};
}

const mealsReceived = (mealsArray) => {
  return {type: Types.MEALS_RECEIVED, mealsArray};
}

const setCurrentMeal = (meal) => {
  return {type: Types.SET_CURRENT_MEAL, meal};
}

const patchCurrentMeal = (mealPatch) => {
  return {type: Types.PATCH_CURRENT_MEAL, mealPatch};
}

const patchMeal = (mealID, patch) => {
  return {type: Types.PATCH_MEAL, mealID, patch};
}

const deleteMeal = (mealID) => {
  return {type: Types.DELETE_MEAL, mealID};
}

const searchDish = (query) => {
  return {type: Types.SEARCH_DISH, query};
}
const receiveSearchResults = (results) => {
  return {type: Types.RECEIVE_SEARCH_RESULTS, results};
}


const submitFeedback = (feedback) => {
  return {type: Types.SUBMIT_FEEDBACK, feedback};
}



/**
 Makes available all the action creators we've created.
 */
export default {
  attemptLogin,
  attemptRegister,
  loginSuccess,
  loginFailure,
  registerFailure,
  logout,
  startup,
  requestTemperature,
  receiveTemperature,
  receiveTemperatureFailure,
  tookPhoto,
  fetchMeals,
  fetchMeal,
  mealsReceived,
  setCurrentMeal,
  patchCurrentMeal,
  patchMeal,
  deleteMeal,
  searchDish,
  receiveSearchResults,
  submitFeedback

}
