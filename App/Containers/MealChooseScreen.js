import React, { PropTypes } from 'react'
import { UIManager, View, ScrollView, Text, LayoutAnimation, DeviceEventEmitter, Image, ListView, TouchableNativeFeedback, Animated, TouchableWithoutFeedback, Alert} from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import { Metrics, Fonts, Colors} from '../Themes'

import ParallaxScrollView from '../Components/ParallaxScrollView'
// external libs
import Icon from 'react-native-vector-icons/MaterialIcons';
import Animatable from 'react-native-animatable'


import GoogleAnalytics from 'react-native-google-analytics-bridge';




// Styles
import styles from './Styles/MealChooseScreenStyle'

// I18n
import I18n from '../I18n/I18n.js'

export default class MealChooseScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      visibleHeight: Metrics.screenHeight,
      search: false,
      rowSelected: 0,
      //mealSelected: 'Schweinebraten',
      fabone: new Animated.Value(6),
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 != r2
      })



    }

    UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);


    this._renderRow = this._renderRow.bind(this)
    this.handlePressFABin = this.handlePressFABin.bind(this)
    this.handlePressFABout = this.handlePressFABout.bind(this)
    this.handlePressFAB = this.handlePressFAB.bind(this)

    this.tapSearch = this.tapSearch.bind(this)
    this.handlePressListItem = this.handlePressListItem.bind(this)


  }


  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    DeviceEventEmitter.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    DeviceEventEmitter.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

    // Configure nav button
    this.props.navigator.state.tapHamburger = () => {
      this.props.navigator.drawer.toggle()
    }
    this.props.navigator.state.tapSearch = this.tapSearch.bind(this)
    this.props.navigator.state.searchOnChangeText = this.searchOnChangeText.bind(this)


    //Some dummy data for list
    this.setState({dataSource: this.state.dataSource.cloneWithRows([['Schweinebraten'],['Knoedel'],['Schweinshax'],['Eisbein'],['Sauerbraten'],['Rinderbraten']])});




    if(!this.props.navigator.state.searchActive) {
      //Yeah. ES6 rocks.
      suggestedDishes = [...this.props.currentMeal.linked_dish, ...this.props.currentMeal.soft_linked_dishes]


      //sorting the classes dictionary
      //this I got from http://stackoverflow.com/questions/5199901/how-to-sort-an-associative-array-by-its-values-in-javascript



      //for (var key in classes) tuples.push([key, classes[key]]);
      var tuples = []
          suggestedDishes.forEach((e, index, array) => {
            tuples.push([e.dish, e.probability])
      })

      tuples.sort(function(a, b) {
          a = a[1];
          b = b[1];

          return a < b ? -1 : (a > b ? 1 : 0);
      });
      tuples.reverse()


      var sortedSuggestions = []

      for (var i = 0; i < tuples.length; i++) {
          var key = tuples[i][0];
          //var value = tuples[i][1];

          sortedSuggestions.push(key)
      }


    }
    else {
      var sortedSuggestions = this.props.searchResults
    }


    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.setState({dataSource: ds.cloneWithRows(sortedSuggestions),})

    this.props.navigator.setState({searchActive:false})


  }

  componentDidMount() {
    GoogleAnalytics.trackScreenView('MealChoose')
  }

  componentWillReceiveProps (nextProps) {
      console.log(nextProps.navigator.state.searchActive)
      if(!nextProps.navigator.state.searchActive) {
        //Yeah. ES6 rocks.
        suggestedDishes = [...nextProps.currentMeal.linked_dish, ...nextProps.currentMeal.soft_linked_dishes]


        //sorting the classes dictionary
        //this I got from http://stackoverflow.com/questions/5199901/how-to-sort-an-associative-array-by-its-values-in-javascript
        var tuples = []
            suggestedDishes.forEach((e, index, array) => {
              tuples.push([e.dish, e.probability])
        })

        tuples.sort(function(a, b) {
            a = a[1];
            b = b[1];

            return a < b ? -1 : (a > b ? 1 : 0);
        });
        tuples.reverse()


        //for (var key in classes) tuples.push([key, classes[key]]);

        var sortedSuggestions = []

        for (var i = 0; i < tuples.length; i++) {
            var key = tuples[i][0];
            //var value = tuples[i][1];

            sortedSuggestions.push(key)
        }


      }
      else {
        var sortedSuggestions = this.props.searchResults
      }


      var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.setState({dataSource: ds.cloneWithRows(sortedSuggestions),})






  }

  tapSearch() {
    //this.setState({search: true})
    this.props.navigator.setState({searchActive:true})
    ///this.props.navigator.push(Routes.CameraScreen)
    //Alert.alert('uhuhuhuh')
  }

  searchOnChangeText(text) {
    const { dispatch } = this.props
    dispatch(Actions.searchDish(text))
  }


  handlePressFAB () {
    const { dispatch } = this.props
    dispatch(Actions.fetchMeal(this.props.currentMeal.id))
    this.props.navigator.push(Routes.MealScreen)
  }

  handlePressFABin () {
    //this.props.navigator.push(Routes.MealScreen)
    Animated.timing(                          // Base: spring, decay, timing
      this.state.fabone,                 // Animate `bounceValue`
      {
        toValue: 12,                         // Bouncier spring
      }
    ).start();
  }
  handlePressFABout () {
    this.props.navigator.push(Routes.MealScreen)
    Animated.timing(                          // Base: spring, decay, timing
      this.state.fabone,                 // Animate `bounceValue`
      {
        toValue: 6,                           // Bouncier spring
      }
    ).start();
  }

  componentWillUnmount () {
    DeviceEventEmitter.removeAllListeners('keyboardDidShow')
    DeviceEventEmitter.removeAllListeners('keyboardDidHide')
  }



  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize
    })
  }

  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight
    })
  }
  handlePressListItem(rowID) {
    this.props.navigator.setState({searchActive:false})

    const { dispatch } = this.props
    dispatch(Actions.patchMeal(this.props.currentMeal.id, {linked_dish: this.state.dataSource.getRowData(0,parseInt(rowID))}))
    //this.setState({rowSelected: parseInt(rowID)});
  }

  _renderHeader() {
    return(
      <View style={{height:48, paddingLeft:16, justifyContent:'center'}}>
        <Text style={{color: Colors.primaryText, opacity:0.54,
        fontSize: Fonts.size.medium,
        fontFamily: Fonts.type.medium}} >Suggestions</Text>
      </View>
    )
  }

  _renderRow(rowData: string, sectionID: number, rowID: number, highlightRow: (sectionID: number, rowID: number) => void) {
    if (true) {
      return(

        <View>

          <TouchableNativeFeedback onPress={() => this.handlePressListItem(rowID)}>
            <View style={{flex:1,height:56,flexDirection:'row',alignItems:'center', paddingLeft:16, paddingRight:16, backgroundColor:rowData==this.props.currentMeal.linked_dish ? Colors.divider : null}}>

              <View style={{width:40,height:40,borderRadius:20,alignItems:'center', justifyContent:'center', backgroundColor:rowData==this.props.currentMeal.linked_dish ? Colors.defaultPrimary : Colors.secondaryText}}>
                <Icon name='restaurant' size={18} color='white' />
              </View>
              <Text style={{
                textAlign: 'left',
                color: Colors.primaryText,
                fontSize: Fonts.size.regular,
                fontFamily: Fonts.type.base,
                marginLeft:16
              }}>{rowData.title}</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      )
  }
  else {
    return (null)
  }
}


  render () {

    return (


      <View style={styles.noPadContainer}>


        <View style={{elevation:0, flex:1, alignItems:'stretch', justifyContent:'center'}}>

          <ParallaxScrollView
          parallaxHeaderHeight={((s) => s?0:Metrics.screenWidth/4*3)(this.props.navigator.state.searchActive)}
          renderBackground={()=>(
            <Image source={{uri: this.props.photo}} style={{justifyContent:'flex-end', width:Metrics.screenWidth, height:Metrics.screenWidth/4*3}} >
            </Image>
          )}
          fadeOutForeground={false}
          renderForeground={()=>(
            this.props.navigator.state.searchActive?
            <View />
            :
            <View style={{flex:1, justifyContent:'flex-end'}}>
              <View style={{backgroundColor:'black',opacity:0.6, justifyContent:'center', paddingLeft:16, paddingTop:16, paddingBottom:16}}>
                <Text style={{color: Colors.textPrimary, fontSize: Fonts.style.display.fontSize,
                fontFamily: Fonts.style.display.fontFamily}}>
                 {this.props.currentMeal.linked_dish.title}
                </Text>
              </View>
              <Text style={{position:'absolute', bottom:16, left:16, color: Colors.textPrimary, fontSize: Fonts.style.display.fontSize,
              fontFamily: Fonts.style.display.fontFamily}}>
               {this.props.currentMeal.linked_dish.title}
              </Text>
            </View>
          )}>

            <ListView
              dataSource={this.state.dataSource}
              renderRow={this._renderRow}
              renderHeader={this._renderHeader}/>

          </ParallaxScrollView>

          <View style={{
            position: 'absolute',
          bottom: 0,
          right: 0,
          }}>
            <TouchableWithoutFeedback onPress={this.handlePressFAB} onPressIn={this.handlePressFABin} onPressOut={this.handlePressFABout}>
              <Animated.View style={[styles.floatingActionButton,{elevation:this.state.fabone}]} >
                <Icon name='done' size={24} color='white' />
              </Animated.View>
            </TouchableWithoutFeedback>


          </View>

      </View>

      </View>





    )
  }
}

const mapStateToProps = (state) => {
  return {
    photo: state.photo.photo,
    currentMeal: state.meals.current,
    searchResults: state.dish.searchResults,
  }
}

export default connect(mapStateToProps)(MealChooseScreen)
