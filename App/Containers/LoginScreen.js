import React, {PropTypes} from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  LayoutAnimation,
  Alert,
  Modal
} from 'react-native'
import { connect } from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import Actions from '../Actions/Creators'
import {Images, Metrics} from '../Themes'
import Routes from '../Navigation/Routes'


import LoginContainer from './LoginContainer'


// I18n
import I18n from '../I18n/I18n.js'

class LoginScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      showModal:false
    }

  }


  componentWillMount () {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))
    if(this.props.username!=null) {
      this.props.navigator.resetTo(Routes.FoodLogScreen)
      this.setState({showModal:false})
    } else{
      this.setState({showModal:true})
    }


  }

  componentWillReceiveProps(np) {
    if(np.username!=null) {
      this.props.navigator.resetTo(Routes.FoodLogScreen)
      this.setState({showModal:false})
    } else{
      this.setState({showModal:true})
    }
  }



  componentWillUnmount () {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize,
      topLogo: {width: 100, height: 70}
    })
  }

  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: Metrics.screenWidth}
    })
  }



  render () {

    return (
      <Modal visible={this.state.showModal}><LoginContainer /></Modal>
    )
  }

}

LoginScreen.propTypes = {
  navigator: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    username: state.login.username,
  }
}

export default connect(mapStateToProps)(LoginScreen)
