import React, { PropTypes } from 'react'
import { View, Text, TouchableNativeFeedback, LayoutAnimation, UIManager } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'

// Styles
import styles from './Styles/BottomBarStyle'
import {Metrics, Colors} from '../Themes'

import Icon from 'react-native-vector-icons/MaterialIcons';


export default class BottomBar extends React.Component {

  constructor (props) {
     super(props)
     this.state = {
       activeRoute: 0
     }

     //UIManager.setLayoutAnimationEnabledExperimental &&   UIManager.setLayoutAnimationEnabledExperimental(true);


  }

  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {
    //LayoutAnimation.easeInEaseOut();



  }



  render () {


      //Huge piece of shit
      var btnstyle = [styles.buttonTextInActive, styles.buttonTextInActive,styles.buttonTextInActive]
      var iconstyle  = [{opacity:0.54}, {opacity:0.54}, {opacity:0.54}]

      btnstyle[this.state.activeRoute] = styles.buttonTextActive
      iconstyle[this.state.activeRoute] = {opacity:1}

      return (
        <View style={styles.barStyle}>
          <TouchableNativeFeedback onPress={() => {
            this.setState({activeRoute: 0})
            this.props.navigator.resetTo(Routes.FoodLogScreen)}
          }>
            <View style={styles.bottomBarButtonContainer} ><Icon name="home" size={24} color='white' style={iconstyle[0]}/>
              <Text style={btnstyle[0]}>Meal Gallery</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback onPress={() => {
            this.setState({activeRoute: 1})
            this.props.navigator.resetTo(Routes.CameraScreen)
          }}>
            <View style={styles.bottomBarButtonContainer} ><Icon name="add-circle" size={24} color='white' style={iconstyle[1]}/>
              <Text style={btnstyle[1]}>Add Meal</Text>
            </View>
          </TouchableNativeFeedback>

          <TouchableNativeFeedback onPress={() => {
            this.setState({activeRoute: 2})
            this.props.navigator.resetTo(Routes.FeedbackScreen)
          }}>
            <View style={styles.bottomBarButtonContainer} ><Icon name="feedback" size={24} color='white' style={iconstyle[2]}/>
              <Text style={btnstyle[2]}>Feedback</Text>
            </View>
          </TouchableNativeFeedback>

        </View>
      )


  }
}

const mapStateToProps = (state) => {
  return {

  }
}

export default connect(mapStateToProps)(BottomBar)
