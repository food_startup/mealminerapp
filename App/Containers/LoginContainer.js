import React, { PropTypes } from 'react'
import { View, TouchableNativeFeedback, Text, TextInput } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'

import {Metrics, Colors, Fonts} from '../Themes'

// Styles
import styles from './Styles/LoginContainerStyle'

import Icon from 'react-native-vector-icons/MaterialIcons';



export default class LoginContainer extends React.Component {

   constructor (props) {
     super(props)
     this.state = {
       showSignUp: false,
       //email:'admin@admin.com',
       //password:'admin'
     }
   }

  componentWillMount () {
    //TODO Make Register and SignUp wording consistent

    this.handlePressLogin = this.handlePressLogin.bind(this)
    this.handlePressRegister = this.handlePressRegister.bind(this)
    this.handlePressGoToSignUp = this.handlePressGoToSignUp.bind(this)
    this.handlePressGoToLogin = this.handlePressGoToLogin.bind(this)
    this.showSignupForm = this.showSignupForm.bind(this)

  }


  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillReceiveProps(nextProps) {
    console.log('updating something')
    this.errorMessage = nextProps.registerFailure?"E-Mail/Password invalid":""
    this.loginErrorMessage = nextProps.loginFailure?"E-Mail/Password combination invalid":""
    //this.errorMessage = 'asdfasdfasdf'
    console.log('lf', nextProps.loginFailure)
    console.log('sf', nextProps.registerFailure)
  }

  handlePressLogin () {
    const { dispatch } = this.props
    this.isAttempting = true
    // attempt a login - a saga is listening to pick it up from here.
    dispatch(Actions.attemptLogin(this.state.email,this.state.password))

  }

  handlePressRegister () {
    const { dispatch } = this.props
    this.isAttempting = true
    // attempt a login - a saga is listening to pick it up from here.
    dispatch(Actions.attemptRegister(this.state.email,this.state.password))

  }

  handlePressGoToSignUp () {
    this.setState({showSignUp: true})
    const { dispatch } = this.props
    dispatch(Actions.loginFailure(false))
  }

  handlePressGoToLogin () {
    this.setState({showSignUp: false})
    const { dispatch } = this.props
    dispatch(Actions.registerFailure(false))

  }

  showLoginForm = () => (
    <View style={{justifyContent:'center', alignItems:'center'}}><View style={{padding:32}}>
      <View style={{alignItems:'center', height:20}}>
        <Text style={styles.errorMessageText}>{this.loginErrorMessage}</Text>
      </View>
      <TextInput
        style={{paddingTop:20, opacity:0.84, color:'white', fontFamily: Fonts.style.title.fontFamily, fontSize: Fonts.size.input,}}
        placeholder="E-Mail"
        keyboardType='email-address'
        returnKeyType='next'
        placeholderTextColor={'white'}
        underlineColorAndroid={'white'}
        selectionColor={Colors.accent}
        selectTextOnFocus={true}

        onChangeText={email => this.setState({email})}
        onSubmitEditing={(event) => {
          this.refs.PWInput.focus();
        }}
      />

    <TextInput
      style={{paddingTop:20, opacity:0.84, color:'white', fontFamily: Fonts.style.title.fontFamily, fontSize: Fonts.size.input,}}
      placeholder="Password"
      ref='PWInput'
      secureTextEntry={true}
      placeholderTextColor={'white'}
      underlineColorAndroid={'white'}

      selectionColor={Colors.accent}
      selectTextOnFocus={true}
      onChangeText={password => this.setState({password})}
    />
    </View>


    <TouchableNativeFeedback onPress={this.handlePressLogin}>
      <View style={[styles.button, {backgroundColor:Colors.accent}]}>
        <Text style={styles.buttonText}>LOGIN</Text>

      </View>
    </TouchableNativeFeedback>
    <Text style={{marginTop:16, color:'white'}}>or</Text>
    <TouchableNativeFeedback onPress={this.handlePressGoToSignUp}>
      <View style={[styles.button]}>
        <Text style={[styles.buttonText, {color:'white', opacity:0.84}]}>CREATE ACCOUNT</Text>

      </View>
    </TouchableNativeFeedback>
    <Text style={{marginTop:30, color:'white'}}>support@mealomi.com</Text>
    </View>
  )




  showSignupForm = () => (
    <View style={{justifyContent:'center', alignItems:'center'}}>

    <View style={{padding:32}}>
      <View style={{alignItems:'center', height:20}}>
        <Text style={styles.errorMessageText}>{this.errorMessage}</Text>
      </View>
      <TextInput
        style={{paddingTop:20, opacity:0.84, color:'white', fontFamily: Fonts.style.title.fontFamily, fontSize: Fonts.size.input,}}
        placeholder="E-Mail (@cdtm.de)"
        keyboardType='email-address'
        returnKeyType='next'
        placeholderTextColor={'white'}
        underlineColorAndroid={'white'}
        onChangeText={email => this.setState({email})}
        selectionColor={Colors.accent}
        selectTextOnFocus={true}
        onSubmitEditing={(event) => {
          this.refs.PWInput.focus();
        }}
      />

    <TextInput
      style={{paddingTop:20, opacity:0.84, color:'white', fontFamily: Fonts.style.title.fontFamily, fontSize: Fonts.size.input,}}
      placeholder="Password (min. 8 letters)"
      ref='PWInput'
      secureTextEntry={true}
      placeholderTextColor={'white'}
      underlineColorAndroid={'white'}
      onChangeText={password => this.setState({password})}
      selectionColor={Colors.accent}
      selectTextOnFocus={true}
    />

    </View>


    <TouchableNativeFeedback onPress={this.handlePressRegister}>
      <View style={[styles.button, {backgroundColor:Colors.accent}]}>
        <Text style={styles.buttonText}>CREATE ACCOUNT</Text>

      </View>
    </TouchableNativeFeedback>
    <Text style={{marginTop:16, color:'white'}}>or</Text>
    <TouchableNativeFeedback onPress={this.handlePressGoToLogin}>
      <View style={[styles.button]}>
        <Text style={[styles.buttonText, {color:'white', opacity:0.84}]}>LOGIN</Text>

      </View>
    </TouchableNativeFeedback>
    <Text style={{marginTop:30, color:'white'}}>support@mealomi.com</Text>
    </View>
  )



  render () {
    if(true){
      if (!this.state.showSignUp) {
        form = this.showLoginForm()
      }
      else {
        form = this.showSignupForm()
      }

      return (
        <View style={{width:Metrics.screenWidth, height:Metrics.screenHeight, backgroundColor:Colors.defaultPrimary, justifyContent:'center', alignItems:'center'}}>
          <View>
            <Text style={{fontSize: 80,
            fontFamily: Fonts.type.banner, color:'white'}}> MealoMi </Text>
          </View>

          {form}

        </View>
      )
    }
    else {
      return (<View />)
    }

  }
}

const mapStateToProps = (state) => {
  return {
    username: state.login.username,
    registerFailure: state.login.registerFailure,
    loginFailure: state.login.loginFailure
  }
}

export default connect(mapStateToProps)(LoginContainer)
