import { StyleSheet } from 'react-native'
import { ApplicationStyles, Elevations, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  halfCardLeft: {
    borderRadius:2,
    elevation:0,
    padding:16,
    marginRight:4
  },
  halfCardRight: {
    borderRadius:2,
    elevation:0,
    padding:16,
    marginLeft:4
  },
  Card: {
    borderRadius:2,
    //elevation:Elevations.Card,
    padding:16,
  },
  Modal: {
    width:280,
    backgroundColor:'white',
    elevation:Elevations.Dialogue
  },
  ModalContent: {
    padding:24
  },
  ModalButtons: {
    height:52,
    padding:8,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  button: {
    borderRadius: 2,
    height: 36,
    paddingLeft:8,
    paddingRight:8,
    marginLeft:8,
    justifyContent:'center'
  },

  buttonText: {
    color: Colors.accent,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.style.buttonText.fontFamily
  },
})
