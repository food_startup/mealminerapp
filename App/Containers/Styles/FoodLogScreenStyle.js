import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  list: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    itemLeft: {
        backgroundColor: Colors.divider,
        marginLeft:0,
        marginRight:2,
        marginBottom:4,
        width: Metrics.screenWidth/2-2,
        height: Metrics.screenWidth/2-2,
    },
    itemRight: {
        backgroundColor: Colors.divider,
        marginLeft:2,
        marginRight:0,
        width: Metrics.screenWidth/2-2,
        height: Metrics.screenWidth/2-2,
    },
    tileFooter: {
      height: 48,
      backgroundColor:'black',
      opacity:0.8,
      paddingLeft:16,
      paddingRight:16,
      justifyContent:'center'
    },
    tileFooterOverlay: {
      position:'absolute',
      bottom:0,
      height:48,
      paddingLeft:16,
      paddingRight:16,
      justifyContent:'center',
      width: Metrics.screenWidth/2-2
    },
    tileText: {
      fontSize:16,
      fontFamily: Fonts.type.bold,
      color:Colors.textPrimary
    }
})
