import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Elevations, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  cameraWindow: {
    flex: 1,
    alignItems:'center',
    justifyContent:'flex-end',
    paddingBottom:20
  },
  bottomBar: {
    position: 'absolute',
    bottom: 0,
    height:100,
    width: Metrics.screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'transparent',
    elevation: Elevations.bottomNavBar
  },
  barButton: {
    height:60,
    flex: 1,
    alignItems:'center',
    justifyContent:'center',
    padding: 16,
    margin: 8
  },
  buttonText: {
    textAlign: 'center',
    color: Colors.textPrimary,
    fontSize: Fonts.style.buttonText.fontSize,
    fontFamily: Fonts.style.buttonText.fontFamily
  }

})
