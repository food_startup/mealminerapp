import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Elevations } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  button: {
    borderRadius: 2,
    height: 44,
    paddingLeft:16,
    paddingRight:16,
    alignItems:'center',
    justifyContent:'center'
  },

  buttonText: {
    textAlign: 'center',
    color: Colors.textPrimary,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.style.buttonText.fontFamily
  },

  Modal: {
    width:280,
    backgroundColor:'white',
    elevation:Elevations.Dialogue
  },
  ModalContent: {
    padding:24
  },
  ModalButtons: {
    height:52,
    padding:8,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  button: {
    borderRadius: 2,
    height: 36,
    paddingLeft:8,
    paddingRight:8,
    marginLeft:8,
    justifyContent:'center'
  },

  buttonTextModal: {
    color: Colors.accent,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.style.buttonText.fontFamily
  },


})
