import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingTop: 70,
    backgroundColor: Colors.defaultPrimary
  },
  form: {
    backgroundColor: Colors.snow,
    margin: 10,
    borderRadius: 4
  },
  row: {
    paddingVertical: 20,
    paddingHorizontal: 20
  },
  rowLabel: {
    color: Colors.charcoal
  },
  textInput: {
    height: 40,
    color: Colors.coal
  },
  textInputReadonly: {
    height: 40,
    color: Colors.steel
  },
  loginRow: {
    paddingBottom: 16,
    paddingHorizontal: 16,
    flexDirection: 'row'
  },
  loginButtonWrapper: {
    justifyContent:'flex-end'
  },
  loginButton: {
    borderRadius:2,
    height: 36,
    paddingLeft:16,
    paddingRight:16,
    marginLeft:8,
    justifyContent:'center',
    backgroundColor: Colors.accent,
  },
  loginText: {
    textAlign: 'center',
    color: Colors.textPrimary
  },
  topLogo: {
    alignSelf: 'center',
    resizeMode: 'contain'
  }
})
