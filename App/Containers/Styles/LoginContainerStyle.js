import { StyleSheet } from 'react-native'
import { Colors, Metrics, ApplicationStyles, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Metrics.navBarHeight,
    backgroundColor: Colors.background,
    elevation:100
  },

  button: {
    borderRadius: 2,
    height: 44,
    paddingLeft:16,
    paddingRight:16,
    alignItems:'center',
    justifyContent:'center'
  },

  buttonText: {
    textAlign: 'center',
    color: Colors.textPrimary,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.style.buttonText.fontFamily
  },

  errorMessageText: {
    color: '#F44336',
    opacity:0.84,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold
  }

})
