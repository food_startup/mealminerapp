import { StyleSheet } from 'react-native'
import { Colors, Metrics, ApplicationStyles, Elevations, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Metrics.navBarHeight,
    backgroundColor: Colors.background
  },
  barStyle: {
    
    width:Metrics.screenWidth,
    backgroundColor:Colors.defaultPrimary,
    elevation:Elevations.bottomNavBar,
    justifyContent:'space-around',
    alignItems:'center',
    flexDirection: 'row'
  },
  bottomBarButtonContainer: {

    height:56,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 6,
    flexDirection:'column',
    flex:1,

    justifyContent:'flex-end',
    alignItems: 'center'
  },
  buttonTextActive: {
    textAlign: 'center',
    color: Colors.textPrimary,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.regular
  },
  buttonTextInActive: {
    textAlign: 'center',
    color: Colors.textPrimary,
    opacity:0.54,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.regular,
    padding:0
  }
})
