import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Elevations } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  footerButtonBar: {
    backgroundColor:Colors.defaultPrimary,
    elevation: Elevations.bottomNavBar,
    height:48,
    flexDirection:'row',
    justifyContent:'flex-end',
    alignItems:'center'
  },
  button : {
    height:36,
    margin: 8,
    padding: 8
  },
  buttonText: {
    textAlign: 'center',
    color: Colors.textPrimary,
    fontSize: Fonts.style.buttonText.fontSize,
    fontFamily: Fonts.style.buttonText.fontFamily
  },
  floatingActionButton: {
    elevation:Elevations.FAB,
    backgroundColor:Colors.accent,
    height:56,
    width:56,
    borderRadius:28,
    marginRight:16,
    marginBottom:16,
    justifyContent:'center',
    alignItems:'center'
  }
})
