import React, { PropTypes } from 'react'
import { View, ScrollView, Text, LayoutAnimation, DeviceEventEmitter, TouchableNativeFeedback } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import { Metrics, Colors } from '../Themes'
// external libs
//import Icon from 'react-native-vector-icons/FontAwesome'
import Animatable from 'react-native-animatable'
import Camera from 'react-native-camera';

import Icon from 'react-native-vector-icons/MaterialIcons';

import ImageResizer from 'react-native-image-resizer';

var ProgressBar = require('ProgressBarAndroid');

import GoogleAnalytics from 'react-native-google-analytics-bridge';






// Styles
import styles from './Styles/CameraScreenStyle'

// I18n
import I18n from '../I18n/I18n.js'

export default class CameraScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      visibleHeight: Metrics.screenHeight,
      flashMode: Camera.constants.FlashMode.auto
    }

    this.onPressTakePhoto = this.onPressTakePhoto.bind(this)
  }

  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {



    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    DeviceEventEmitter.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    DeviceEventEmitter.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

    // Configure nav button
    this.props.navigator.state.tapFlash = () => {
        switch(this.props.navigator.state.flashState) {
          case "AUTO":
            this.props.navigator.setState({flashState:'ON'})
            this.setState({flashMode: Camera.constants.FlashMode.on})
            break;
          case "ON":
            this.props.navigator.setState({flashState:'OFF'})
            this.setState({flashMode: Camera.constants.FlashMode.off})
            break;
          case "OFF":
            this.props.navigator.setState({flashState:'AUTO'})
            this.setState({flashMode: Camera.constants.FlashMode.auto})
            break;
          default:
            this.props.navigator.setState({flashState:'AUTO'})
            this.setState({flashMode: Camera.constants.FlashMode.auto})
        }
      }
    this.props.navigator.state.tapFlash = this.props.navigator.state.tapFlash.bind(this)

    this.setState({isFetching:false})



  }

  componentDidMount() {
    GoogleAnalytics.trackScreenView('Camera')
  }

  componentWillUnmount () {

    DeviceEventEmitter.removeAllListeners('keyboardDidShow')
    DeviceEventEmitter.removeAllListeners('keyboardDidHide')
  }

  componentWillUpdate(np, ns) {

    if(ns.isFetching && np.currentMeal) {
      console.log('hurray, loading done')
      this.setState({isFetching:false})
      this.props.navigator.push(Routes.MealChooseScreen)
    }
  }


  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize
    })
  }

  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight
    })
  }

  onPressTakePhoto() {
    const { dispatch } = this.props
    console.log('taking photo')
    this.camera.capture().then((data) => {
      ImageResizer.createResizedImage(data.path, Metrics.screenWidth, Metrics.screenHeight, 'JPEG', 100).then(((resizedImageUri) => {
        // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
        dispatch(Actions.setCurrentMeal(null))
        this.setState({isFetching:true})
        dispatch(Actions.tookPhoto(resizedImageUri))
      }).bind(this)).catch((err) => {
        // Oops, something went wrong. Check that the filename is correct and
        // inspect err to get more details.
      });

    })


  }

  render () {
    return (
      <View style={styles.noPadContainer}>
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.cameraWindow}
          aspect={Camera.constants.Aspect.fill}
          captureTarget={Camera.constants.CaptureTarget.disk}
          flashMode={this.state.flashMode}>


        </Camera>
        {(() => {
          if(this.state.isFetching) {
            return (
              <View style={{position:'absolute', flex:1 , top:0, left:0, width:Metrics.screenWidth, height:Metrics.screenHeight-56-56, alignItems:'center', justifyContent:'center', backgroundColor:'transparent'}}>
                <ProgressBar styleAttr="Large"/>
              </View>
            )
          }
          else {
            return (<View />)
          }
        })()}

        <View style={styles.bottomBar}>



          <TouchableNativeFeedback onPress={this.onPressTakePhoto.bind(this)}>
            <View style={{ height: 60, marginRight: 10, marginLeft: 10}}>
                <Icon name="photo-camera" size={60} color='white' />
            </View>
          </TouchableNativeFeedback>

        </View>
      </View>
    )
  }
}



CameraScreen.propTypes = {
  dispatch: PropTypes.func
}

const mapStateToProps = (state) => {
  return {
    currentMeal: state.meals.current,
  }
}

export default connect(mapStateToProps)(CameraScreen)
