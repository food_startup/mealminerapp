import React, { PropTypes } from 'react'
import { View, Text, LayoutAnimation, DeviceEventEmitter, TextInput, TouchableNativeFeedback, Modal } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import { Metrics, Colors } from '../Themes'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import Animatable from 'react-native-animatable'

// Styles
import styles from './Styles/FeedbackScreenStyle'

import GoogleAnalytics from 'react-native-google-analytics-bridge';


// I18n
import I18n from '../I18n/I18n.js'

export default class FeedbackScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      visibleHeight: Metrics.screenHeight,
      showConfirmDialogue: false,
      keyboardShow: false
    }

  }

  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    DeviceEventEmitter.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    DeviceEventEmitter.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

    this.handlePressConfirm = this.handlePressConfirm.bind(this)
    this.handleSubmitFeedback = this.handleSubmitFeedback.bind(this)
  }

  componentDidMount() {
    console.log('focusing')
    this.refs.FeedbackForm.focus();
    GoogleAnalytics.trackScreenView('Feedback')
  }

  componentWillUnmount () {
    DeviceEventEmitter.removeAllListeners('keyboardDidShow')
    DeviceEventEmitter.removeAllListeners('keyboardDidHide')
  }

  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize,
      keyboardShow: true
    })
    console.log(this.state.visibleHeight)
  }

  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight,
      keyboardShow: false
    })
  }

  handlePressConfirm () {
    this.setState({showConfirmDialogue:false})
    this.refs.FeedbackForm.clear();

  }

  handleSubmitFeedback () {
    this.setState({showConfirmDialogue:true})
    const { dispatch } = this.props
    dispatch(Actions.submitFeedback(this.state.text))
  }

  render () {
    //TODO remove this truly awful hack
    if(this.state.keyboardShow) {
      var h = this.state.visibleHeight-56-56
    }
    else {
      var h = this.state.visibleHeight-56-56-56
    }
    return (
      <View style={[styles.noPadContainer,{padding:8, paddingTop:8}]}>
      <View style={{height:h}}>

          <Modal
            transparent={true}
            animationType='fade'
            visible={this.state.showConfirmDialogue}
            onRequestClose={() => {this.setState({showConfirmDialogue:false})}}>

          <View style={{flex:1, backgroundColor:'black', opacity:0.54}}/>
          <View style={{flex:1, position:'absolute', top:0, left:0, width:Metrics.screenWidth, height:Metrics.screenHeight, justifyContent:'center', alignItems:'center'}}>
            <View style={styles.Modal}>
              <View style={styles.ModalContent}>
                <Text style={[styles.buttonText, {color:'black',opacity:0.54}]}>Thanks for your feedback</Text>
              </View>
              <View style={styles.ModalButtons}>

                <TouchableNativeFeedback onPress={this.handlePressConfirm}>
                  <View style={styles.button}>
                    <Text style={styles.buttonTextModal}>CONTINUE</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>

            </View>
          </View>
        </Modal>



        <TextInput
          style={{flex:1, borderColor: 'blue', borderWidth: 10, textAlignVertical:'top'}}
          onChangeText={(text) => this.setState({text})}
          multiline={true}
          placeholder="Type Feedback"
          underlineColorAndroid='white'
          ref='FeedbackForm'
        />
        <View>
        <TouchableNativeFeedback onPress={this.handleSubmitFeedback}>
          <View style={[styles.button, {backgroundColor:Colors.accent}]}>
            <Text style={styles.buttonText}>SUBMIT FEEDBACK</Text>

          </View>
        </TouchableNativeFeedback>
        </View>
      </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

export default connect(mapStateToProps)(FeedbackScreen)
