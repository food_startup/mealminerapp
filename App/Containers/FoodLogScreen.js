import React, { PropTypes } from 'react'
import { View, ScrollView, Text, LayoutAnimation, DeviceEventEmitter, ListView, Image, TouchableNativeFeedback } from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import { Metrics, Colors, Fonts } from '../Themes'
// external libs
import Animatable from 'react-native-animatable'

// Styles
import styles from './Styles/FoodLogScreenStyle'

import Icon from 'react-native-vector-icons/MaterialIcons';

import GoogleAnalytics from 'react-native-google-analytics-bridge';


// I18n
import I18n from '../I18n/I18n.js'

export default class FoodLogScreen extends React.Component {

  constructor (props) {
    super(props)



    this.state = {
      visibleHeight: Metrics.screenHeight
    }
  }

  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    DeviceEventEmitter.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    DeviceEventEmitter.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

    // Configure nav button
    this.props.navigator.state.tapProfile = () => {
      //this.props.navigator.push(Routes.EditProfileScreen)
      const { dispatch } = this.props
      dispatch(Actions.mealsReceived([]))
      dispatch(Actions.logout())
      this.props.navigator.push(Routes.LoginScreen)
    }


    this.setState({isFetching:false})


    const { dispatch } = this.props
    // attempt a login - a saga is listening to pick it up from here.
    console.log('fetching meals')
    dispatch(Actions.fetchMeals())


      var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      //this is massive crap. Using array.slice.reverse() complains due to immutability of this.props.mealsLog
      var arr = []
      for (var i=this.props.mealsLog.length-1; i>=0; i--) {
        arr.push(this.props.mealsLog[i])
      }
      this.setState({dataSource: ds.cloneWithRows(arr),})


    this.onPressTile = this.onPressTile.bind(this)


  }

  componentDidMount() {
    GoogleAnalytics.trackScreenView('FoodLog')
  }

  componentWillUnmount () {
    DeviceEventEmitter.removeAllListeners('keyboardDidShow')
    DeviceEventEmitter.removeAllListeners('keyboardDidHide')
  }


  componentWillReceiveProps(np) {


          console.log('updating mealslog')
          var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
          //this is massive crap. Using array.slice.reverse() complains due to immutability of this.props.mealsLog
          var arr = []
          for (var i=np.mealsLog.length-1; i>=0; i--) {
            arr.push(np.mealsLog[i])
          }
          this.setState({dataSource: ds.cloneWithRows(arr),})

  }


  componentWillUpdate(np, ns) {

    if(ns.isFetching && np.currentMeal) {
      console.log('hurray, loading done')
      this.setState({isFetching:false})
      this.props.navigator.push(Routes.MealScreen)
    }
  }

  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize
    })
  }


  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight
    })
  }

  onPressTile(meal) {
    console.log('hallo',meal)
    const { dispatch } = this.props
    //haesslich aber geht
    dispatch(Actions.setCurrentMeal(null))
    dispatch(Actions.fetchMeal(meal.id))
    this.setState({isFetching:true})
    //this.props.navigator.push(Routes.MealScreen)

  }

  render () {
    console.log('ds',this.props.mealsLog.length)
    if (this.props.mealsLog.length != 0) {
      return (


        <View style={styles.noPadContainer}>

          <ListView contentContainerStyle={styles.list}
              dataSource={this.state.dataSource}
              onTouchMove={(e)=>{
                  _listViewDirtyPressEnabled = false;
              }}
              onTouchEnd={(e)=>{
                  _listViewDirtyPressEnabled = true;
              }}
              renderRow={(rowData, sectionID, rowID) => {
                if (parseInt(rowID) % 2 ) {
                  var itemStyle=styles.itemRight;
                } else {
                  var itemStyle=styles.itemLeft;
                }

                switch(rowData.score) {
                  case 'unhealthy':
                    tileFooterStyle = [styles.tileFooter,{backgroundColor:'#B71C1C'}]
                    break;
                  case 'unknown':
                    tileFooterStyle = [styles.tileFooter,{backgroundColor:'#F57F17'}]
                    break;
                  case 'healthy':
                    tileFooterStyle = [styles.tileFooter,{backgroundColor:'#1B5E20'}]
                    break;
                  default:
                    tileFooterStyle = [styles.tileFooter,{backgroundColor:'black'}]
                }



                return(
                  <TouchableNativeFeedback onPress={()=>{
                    //if( !_listViewDirtyPressEnabled ) return ;
                    this.onPressTile(rowData)
                    console.log('boing', rowData)
                  }}>
                  <View style={itemStyle}>
                    <Image source={{uri: rowData.images[0].thumbnail}} style={{flex:1, justifyContent:'flex-end'}}>
                      <View style={tileFooterStyle} />
                      <View style={styles.tileFooterOverlay}>
                        <Text style={styles.tileText}>{rowData.name}</Text>
                      </View>
                    </Image>
                  </View>
                  </TouchableNativeFeedback>
                )
              }}
            />
        </View>
      )
    }
    else {
      return (
        <View style={styles.noPadContainer}>
        <View style={{width:Metrics.screenWidth, height:Metrics.screenHeight-56, justifyContent:'flex-start', alignItems:'center'}}>

            <Text style={{marginTop:48, fontFamily:Fonts.style.display.fontFamily, fontSize:Fonts.style.display.fontSize}}>Welcome to</Text>
            <Text style={{marginTop:24, fontSize: 80,
            fontFamily: Fonts.type.banner, color:Colors.defaultPrimary}}> MealoMi </Text>
        <Text style={{marginTop:16, fontFamily:Fonts.style.bodyTwo.fontFamily, fontSize:Fonts.style.bodyTwo.fontSize}}>Your Personal Food Companion</Text>

        </View>
        <View style={{position:'absolute', bottom:0, alignItems:'center', width:Metrics.screenWidth}}>
          <Text style={{marginTop:16, fontFamily:Fonts.style.bodyTwo.fontFamily, fontSize:Fonts.style.bodyTwo.fontSize}}>Take a photo of your meal to get started</Text>
          <Icon name="arrow-downward" size={42} color='black' style={{opacity:0.57}}/>
        </View>
        </View>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    mealsLog: state.meals.log,
    currentMeal: state.meals.current,
  }
}

export default connect(mapStateToProps)(FoodLogScreen)
