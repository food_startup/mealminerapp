import React, { PropTypes } from 'react'
import { View, ScrollView, Text, LayoutAnimation, DeviceEventEmitter, Image, Modal, TouchableNativeFeedback, ListView} from 'react-native'
import { connect } from 'react-redux'
import Actions from '../Actions/Creators'
import Routes from '../Navigation/Routes'
import { Metrics, Colors, Fonts, Evaluations } from '../Themes'
import ParallaxScrollView from '../Components/ParallaxScrollView'

import PieChart from '../Components/PieChart'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import Animatable from 'react-native-animatable'

// Styles
import styles from './Styles/MealScreenStyle'

import GoogleAnalytics from 'react-native-google-analytics-bridge';


// I18n
import I18n from '../I18n/I18n.js'

export default class MealScreen extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      visibleHeight: Metrics.screenHeight,
      showDeleteDialogue: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 != r2
      })
    }
  }

  static propTypes = {
    navigator: PropTypes.object.isRequired
  }

  componentWillMount () {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    DeviceEventEmitter.addListener('keyboardDidShow', this.keyboardDidShow.bind(this))
    DeviceEventEmitter.addListener('keyboardDidHide', this.keyboardDidHide.bind(this))

    // Configure nav button
    this.props.navigator.state.tapHamburger = () => {
      this.props.navigator.drawer.toggle()
    }

    this.props.navigator.state.tapCheck = {
      handleDone: () => {
        console.log('tap check')

        this.props.navigator.resetTo(Routes.FoodLogScreen)
      },
      handleDelete: () => {
        this.setState({showDeleteDialogue:true})
      }
    }

    this.props.navigator.state.tapCheck.handleDone = this.props.navigator.state.tapCheck.handleDone.bind(this)
    this.handlePressDelete = this.handlePressDelete.bind(this)

    //Shouldn't be doubled. But if it doesn't work here there's something wrong anyway.
    tags = this.props.currentMeal.tags.map((e,i) => e.identifier)
    console.log('tags', tags)
    this.setState({dataSource: this.state.dataSource.cloneWithRows(tags)});


  }

  componentDidMount() {
    GoogleAnalytics.trackScreenView('MealDetail')
  }

  componentWillUnmount () {
    DeviceEventEmitter.removeAllListeners('keyboardDidShow')
    DeviceEventEmitter.removeAllListeners('keyboardDidHide')
  }

  componentWillReceiveProps(nextProps) {
    console.log('test')
    tags = nextProps.currentMeal.linked_dish.tags.map((e,i) => e.identifier)
    console.log('tags', tags)
    this.setState({dataSource: this.state.dataSource.cloneWithRows(tags)});
  }

  keyboardDidShow (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize
    })
  }

  keyboardDidHide (e) {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight
    })
  }

  handlePressDelete = () => {

    const { dispatch } = this.props
    dispatch(Actions.deleteMeal(this.props.currentMeal.id))
    this.setState({showDeleteDialogue:false})
    this.props.navigator.resetTo(Routes.FoodLogScreen)

  }
//transform: [{rotate: '50deg'}]
  detailView() {
    return (
      <View style={{backgroundColor:'white', flex:1, padding:8, flexDirection:'column', justifyContent:'flex-start'}}>
        <View style={{flexDirection:'row', flex:1, alignItems:'stretch'}}>
          <View style={[styles.halfCardLeft,{flex:1, backgroundColor:'white', }]}>
            <Text style={{opacity:0.54, color: Colors.primaryText, fontSize: Fonts.style.bodyTwo.fontSize,
            fontFamily: Fonts.style.bodyTwo.fontFamily}}>Health Points</Text>
            <View style={{height:120, overflow:'hidden', justifyContent:'flex-start', alignItems:'center', marginTop:8}}>

              <PieChart size={120} value={this.props.currentMeal.health_points}/>


            </View>

          </View>
          <View style={[styles.halfCardRight,{flex: 1, backgroundColor:'white'}]}>
            <Text style={{opacity:0.54, color: Colors.primaryText, fontSize: Fonts.style.bodyTwo.fontSize,
            fontFamily: Fonts.style.bodyTwo.fontFamily}}>Tags</Text>

            <View style={{marginTop:8}}>
              <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData) =>
                  <View style={{marginBottom:4}}>
                    <Text style={{flexDirection:'row', opacity:0.87, textAlignVertical:'center', textAlign:'left', color: Colors.primaryText, fontSize: Fonts.style.bodyTwo.fontSize,
                    fontFamily: Fonts.style.bodyTwo.fontFamily}}>{'#' + rowData}</Text>
                  </View>}
              />

            </View>
          </View>




        </View>



        <View style={[styles.Card,{paddingLeft:0, paddingRight:0, height:8*12 , margin:16, backgroundColor:'white'}]}>
          <Text style={{marginBottom:8, opacity:0.54, color: Colors.primaryText, fontSize: Fonts.style.bodyTwo.fontSize,
          fontFamily: Fonts.style.bodyTwo.fontFamily}}>Caloric values per portion</Text>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{flex:1, justifyContent:'center', alignItems:'center', borderColor:Colors.divider, borderRightWidth:1}}>

            <Text>{this.props.currentMeal.linked_dish.nutrients.calories} kcal</Text>
          </View>
          <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>

            <Text>{4.184*this.props.currentMeal.linked_dish.nutrients.calories} kJ</Text>
          </View>


          </View>

        </View>

        <View style={[styles.Card,{paddingLeft:0, paddingRight:0, height:8*12 , margin:16, backgroundColor:'white'}]}>
          <Text style={{marginBottom:8, opacity:0.54, color: Colors.primaryText, fontSize: Fonts.style.bodyTwo.fontSize,
          fontFamily: Fonts.style.bodyTwo.fontFamily}}>Nutrients per portion</Text>
          <View style={{flex:1, flexDirection:'row'}}>
            <View style={{flex:1, justifyContent:'center', alignItems:'center', borderColor:Colors.divider, borderRightWidth:1}}>
              <Text>fats</Text>
              <Text>{this.props.currentMeal.linked_dish.nutrients.fats} g</Text>
            </View>
            <View style={{flex:1, justifyContent:'center',alignItems:'center', borderColor:Colors.divider, borderRightWidth:1}}>
              <Text>proteins</Text>
              <Text>{this.props.currentMeal.linked_dish.nutrients.proteins} g</Text>
            </View>
            <View style={{flex:1, justifyContent:'center', alignItems:'center', borderColor:Colors.divider, borderRightWidth:1}}>
              <Text>carbs</Text>
              <Text>{this.props.currentMeal.linked_dish.nutrients.carbs} g</Text>
            </View>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text>sugars</Text>
              <Text>{this.props.currentMeal.linked_dish.nutrients.sugars} g</Text>
            </View>

          </View>

        </View>




      </View>
    )
  }


  render () {
    return (


      <View style={styles.noPadContainer}>

        <Modal
          transparent={true}
          animationType='fade'
          visible={this.state.showDeleteDialogue}
          onRequestClose={() => {this.setState({showDeleteDialogue:false})}}>
        <View style={{flex:1, backgroundColor:'black', opacity:0.54}}/>
        <View style={{flex:1, position:'absolute', top:0, left:0, width:Metrics.screenWidth, height:Metrics.screenHeight, justifyContent:'center', alignItems:'center'}}>
          <View style={styles.Modal}>
            <View style={styles.ModalContent}>
              <Text style={[styles.buttonText, {color:'black',opacity:0.54}]}>Delete meal?</Text>
            </View>
            <View style={styles.ModalButtons}>
              <TouchableNativeFeedback onPress={() => {this.setState({showDeleteDialogue:false})}}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>CANCEL</Text>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback onPress={this.handlePressDelete}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>DELETE</Text>
                </View>
              </TouchableNativeFeedback>
            </View>

          </View>
        </View>
      </Modal>

        <ParallaxScrollView
        parallaxHeaderHeight={Metrics.screenWidth/4*3}
        renderBackground={()=>(
          <Image source={{uri: this.props.currentMeal.images[0].image}} style={{justifyContent:'flex-end', width:Metrics.screenWidth, height:Metrics.screenWidth/4*3}} >
          </Image>
        )}
        fadeOutForeground={false}
        renderForeground={()=>(
          <View style={{flex:1, justifyContent:'flex-end'}}>
            <View style={{backgroundColor:'black',opacity:0.6, justifyContent:'center', paddingLeft:16, paddingTop:16, paddingBottom:16}}>
              <Text style={{color: Colors.textPrimary, fontSize: Fonts.style.display.fontSize,
              fontFamily: Fonts.style.display.fontFamily}}>
               {this.props.currentMeal.linked_dish.title}
              </Text>
            </View>
            <Text style={{position:'absolute', bottom:16, left:16, color: Colors.textPrimary, fontSize: Fonts.style.display.fontSize,
            fontFamily: Fonts.style.display.fontFamily}}>
             {this.props.currentMeal.linked_dish.title}
            </Text>
          </View>
        )}>

          {(() => this.detailView())()}

        </ParallaxScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    photo: state.photo.photo,
    currentMeal: state.meals.current
  }
}

export default connect(mapStateToProps)(MealScreen)
